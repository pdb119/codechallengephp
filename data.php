<?php

// HEADERS
require ("PHPMailer-master/PHPMailerAutoload.php");
header ( 'Access-Control-Allow-Origin: *' );
header ( "Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
/* header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); */
header ( "Cache-Control: no-store, no-cache, must-revalidate" );
header ( "Cache-Control: post-check=0, pre-check=0", false );
header ( "Pragma: no-cache" );

// the array which will be turned into JSON for javascript reading
$returnData = array ();

// PARAMETER SETUP
$params = array ();
$params ["date"] = $params ["subject"] = $params ["description"] = $params ["action"] = $params ["buildingId"] = $params ["roomId"] = $params ["floor"] = $params ["seats"] = $params ["startTime"] = $params ["endTime"] = $params ["country"] = $params ["state"] = $params ["city"] = $params ["reservationId"] = $params ["userId"] = $params ["password"] = null;
foreach ( $params as $key => $value ) {
	// if (isset($_REQUEST[$key])) $params[$key] = preg_replace("/[^A-Za-z0-9]/", "", $_REQUEST[$key]);
	if($key == "userId" && isset ( $_REQUEST [$key] )){
		$params[$key] = strtolower($_REQUEST [$key]);
	} else {
		if (isset ( $_REQUEST [$key] ))
			$params [$key] = $_REQUEST [$key];
	}
	// if (empty($params[$key])) $params[$key] = "";
}
$userId = $_COOKIE ["uid"];

// SQL CONNECTION
$conn_array = array (
		"UID" => "reservation@g4zdkl71o6",
		"PWD" => "Interns2014",
		"Database" => "reservation",
		"Encrypt" => 1,
		"TrustServerCertificate" => 1 
);
$conn = sqlsrv_connect ( 'tcp:g4zdkl71o6.database.windows.net,1433', $conn_array );

// BEGIN MAIN FUNCTIONS
switch ($params ["action"]) {
	
	case "login" :
		$p = array (
				$params ["userId"] 
		);
		$query = "SELECT * FROM UserAccount WHERE userId=?";
		$user = oneRow ( $conn,$query, $p );
		if ($user) {
			setcookie ( "uid", $user ['userId'], time () + 10800 );
			$returnData ["success"] = true;
		} else {
			//since this isn't in production, we'll create an account if they don't have one
				$query = "INSERT INTO UserAccount (userId,firstName,lastName,password,email) VALUES(?,?,?,?,?);";
				$p = array (
						$params ["userId"],
						$params ["userId"],
						$params ["userId"],
						"password",
						$params ["userId"] . "@att.com"
				);
				$result = sqlsrv_query($conn,$query,$p);
				if(!$result){
			$returnData ["success"] = false;
				} else {
					setcookie ( "uid", $params["userId"], time () + 10800 );
					$returnData ["success"] = true;
				}
		}
		break;
	
	case "getCurrentUser" :
		// $returnData['currentUser'] = $_COOKIE['userId'];
		$returnData ['currentUser'] = $userId;
		break;
	
	case "getAvailable" :
		if (strlen ( $params ["startTime"] ) == 5)
			$params ["startTime"] = $params ["startTime"] . ":00";
		if (strlen ( $params ["endTime"] ) == 5)
			$params ["endTime"] = $params ["endTime"] . ":00";
		$startDateTime = $params ["date"] . " " . $params ["startTime"];
		$endDateTime = $params ["date"] . " " . $params ["endTime"];
		// GET parameters: building (string), floor (int), intime, outtime, seats
		// return roomId, name, seats
		$p = array (
				$startDateTime,
				$endDateTime,
				$params ["buildingId"],
				$params ["floor"],
				$params ["seats"] 
		);
		$query = "EXEC emptyRooms @start=?, @end=?, @building=?, @floor=?, @seats=?";
		// $query = "CREATE TABLE #roomFilter (roomId nvarchar(8) PRIMARY KEY, buildingId int, floor int, seats int, roomName nvarchar(50), x int, y int, width int, height int); INSERT INTO #roomFilter (SELECT roomId, buildingId, floor, seats, roomName, x, y, width, height FROM Room WHERE buildingId=1 AND floor=1 AND seats>=1); SELECT * FROM #roomFilter;";
		// $query = "CREATE TABLE #roomFilter (roomId nvarchar(8) PRIMARY KEY); INSERT INTO #roomFilter (roomId) VALUES('rt556'); SELECT * FROM #roomFiler;";
		// $query = "SELECT roomId, buildingId, floor, seats, roomName, x, y, width, height FROM Room WHERE buildingId=1 AND floor=1 AND seats>=1";
		// echo($query);
		$result = sqlsrv_query ( $conn, $query, $p );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ['availableRooms'] [$i] = $row;
			$i ++;
		}
		break;
	
	case "getRooms" :
		// GET parameters: building, floor
		// return: roomid, name, seats
		$p = array (
				$params ["buildingId"],
				$params ["floor"] 
		);
		$query = "SELECT roomId, roomName, seats, x, y, width, height FROM Room WHERE buildingId=? AND floor=?";
		$result = sqlsrv_query ( $conn, $query, $p );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ['rooms'] [$i] = $row;
			$i ++;
		}
		break;
	
	case "getBuildings" :
		// GET parameters: building, floor
		// return: roomid, name, seats
		$p = array (
				$params ["country"],
				$params ["state"],
				$params ["city"] 
		);
		$query = "SELECT buildingId, buildingName FROM Building WHERE country=? AND state=? AND city=?";
		$result = sqlsrv_query ( $conn, $query, $p );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ['buildings'] [$i] = $row;
			$i ++;
		}
		break;
	
	case "getBuilding" :
		$p = array (
				$params ["buildingId"] 
		);
		$query = "SELECT buildingId, buildingName, floors FROM Building WHERE buildingId=?";
		$returnData = oneRow ($conn, $query, $p );
		break;
	
	case "getLocations" :
		$query = "SELECT DISTINCT country, state, city FROM Building";
		$result = sqlsrv_query ( $conn, $query );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ['locations'] [$i] = $row;
			$i ++;
		}
		break;
	
	case "getRoomDetails" :
		// GET param: roomid
		// return:
		$p = array (
				$params ["roomId"] 
		);
		$query = "SELECT * FROM Room WHERE roomId=?";
		$resultArray = oneRow ( $conn,$query, $p );
		// $returnData['roomId'] = $resultArray['roomId'];
		$returnData = $resultArray;
		// $returnData['roomId'] = $params["roomId"];
		// $returnData['roomName'] = $resultArray['roomName'];
		// $returnData['seats'] = $resultArray['seats'];
		// $returnData['phoneNum'] = $resultArray['phoneNum'];
		// $returnData['printerAddress'] = $resultArray['printerAddress'];
		$query2 = "SELECT Feature.name FROM RoomFeature JOIN Feature ON RoomFeature.featureId=Feature.featureId WHERE RoomFeature.roomId=?";
		$result2 = sqlsrv_query ( $conn, $query2, $p );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result2, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ['features'] [$i] = $row;
			$i ++;
		}
		break;
	
	case "myReservations" :
		// GET parameters: userid
		// return: room name, roomid, building, city, state, intime, outtime
		$p = array (
				$userId,
				date ( "Y-m-d h:i:s" ) 
		);
		$query = "SELECT Reservation.reservationId, Room.roomName, Reservation.roomId, Building.buildingName, Building.city, Building.state, Reservation.subject, Reservation.startTime, Reservation.endTime FROM Reservation JOIN Room ON Reservation.roomId=Room.roomId JOIN Building ON Building.buildingId=Room.buildingId WHERE Reservation.userId=? AND Reservation.endTime>? ORDER BY startTime ASC"; // not sure where the current user's id will be
		$result = sqlsrv_query ( $conn, $query, $p );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ["reservations"] [$i] = $row;
			$i ++;
		}
		break;
	
	case "getReservationDetails" :
		$p = array (
				$params ["reservationId"] 
		);
		$query = "SELECT Reservation.userId,Reservation.roomId,Reservation.reservationId,Reservation.subject,Reservation.description,Reservation.startTime,Reservation.endTime,Room.roomName,Room.seats,Building.buildingName,Building.city,Building.state FROM Reservation JOIN Room ON Room.roomId=Reservation.roomId JOIN Building ON Building.buildingId=Room.buildingId WHERE reservationId=?";
		$returnData = oneRow ( $conn,$query, $p );
		$query2 = "SELECT UserAccount.userId, UserAccount.firstName, UserAccount.lastName FROM ReservationUser JOIN UserAccount ON ReservationUser.userId=UserAccount.userId WHERE ReservationUser.reservationId=?";
		$result2 = sqlsrv_query ( $conn, $query2, $p );
		$i = 0;
		$returnData ['attendees'] = [ ];
		while ( $row = sqlsrv_fetch_array ( $result2, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ['attendees'] [$i] = $row;
			$i ++;
		}
		break;
	
	case "myMeetings" :
		// GET parameters: userid
		// return: room name, roomid, building, city, state, intime, outtime
		$p = array (
				$userId,
				date ( "Y-m-d h:i:s" ) 
		);
		$query = "SELECT Reservation.reservationId, Reservation.subject, Reservation.roomId, Room.roomName, Building.buildingName, Building.city, Building.state, Reservation.startTime, Reservation.endTime FROM ReservationUser JOIN Reservation ON ReservationUser.reservationId=Reservation.reservationId JOIN Room ON Reservation.roomId=Room.roomId JOIN Building ON Building.buildingId=Room.buildingId WHERE ReservationUser.userId=? AND Reservation.endTime>? ORDER BY startTime ASC"; // not sure where the current user's id will be
		$result = sqlsrv_query ( $conn, $query, $p );
		$i = 0;
		while ( $row = sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC ) ) {
			$returnData ["meetings"] [$i] = $row;
			$i ++;
		}
		break;
	
	case "addReservation" :
		// GET parameters: userid(unless stored with session or cookie), roomid, intime, outtime, subject, description
		// return: reservationid
		if (strlen ( $params ["startTime"] ) == 5)
			$params ["startTime"] = $params ["startTime"] . ":00";
		if (strlen ( $params ["endTime"] ) == 5)
			$params ["endTime"] = $params ["endTime"] . ":00";
		$startDateTime = $params ["date"] . " " . $params ["startTime"];
		$endDateTime = $params ["date"] . " " . $params ["endTime"];
		$p = array (
				$userId,
				$params ["roomId"],
				$startDateTime,
				$endDateTime,
				$params ["subject"],
				$params ["description"] 
		);
		sqlsrv_begin_transaction ( $conn );
		$query = "INSERT INTO Reservation (userId, roomId, startTime, endTime, subject, description) VALUES(?,?,?,?,?,?); SELECT SCOPE_IDENTITY();";
		$result = sqlsrv_query ( $conn, $query, $p );
		if (! $result) {
			$returnData ["success"] = false;
			$returnData ['reservationId'] = null;
			sqlsrv_rollback ( $conn );
		} else {
			$query = "INSERT INTO ReservationUser (reservationId, userId) VALUES(?,?);";
			sqlsrv_next_result ( $result );
			sqlsrv_fetch ( $result );
			$returnData ['reservationId'] = sqlsrv_get_field ( $result, 0 );
			$p = array (
					$returnData ['reservationId'],
					$userId 
			);
			$result = sqlsrv_query ( $conn, $query, $p );
			if (! $result) {
				$returnData ["success"] = false;
				$returnData ['reservationId'] = null;
				sqlsrv_rollback ( $conn );
			} else {
				$returnData ["success"] = true;
				sqlsrv_commit ( $conn );
			}
		}
		break;
	
	case "sendEmail" :
		$emailId = $params ["userId"] . "---" . $params ["reservationId"];
		$link = "http://res.azurewebsites.net/emailResponse.php?emailId=" . $emailId;
		$query = "SELECT * FROM UserAccount WHERE userId=?";
		$p = array (
				$params ["userId"]
		);
		$resultData = oneRow($conn,$query,$p);
		if (! $resultData) {
			$query = "INSERT INTO UserAccount (userId,firstName,lastName,password,email) VALUES(?,?,?,?,?);";
			$p = array (
					$params ["userId"],
					$params ["userId"],
					$params ["userId"],
					"password",
					$params ["userId"] . "@att.com" 
			);
			$emailAddress = $params ["userId"] . "@att.com";
			sqlsrv_query ( $conn, $query, $p );
		} else {
			$emailAddress = $resultData ['email'];
		}
		$p = array (
				$emailId,
				$params ["userId"],
				$params ["reservationId"],
				0 
		);
		$query = "INSERT INTO EmailId (emailId, userId, reservationId, accepted) VALUES(?,?,?,?);";
		$result = sqlsrv_query ( $conn, $query, $p );
		$mail = new PHPMailer ();
		$mail->isSMTP ();
		$mail->Host = 'email-smtp.us-east-1.amazonaws.com';
		$mail->SMTPAuth = true;
		$mail->Username = 'AKIAJIBSZBWVHSVR7FSQ';
		$mail->Password = 'AlhZXOPRBLqzJncqovjyb2WeFBjIPHAc9A93zI1/gx/U';
		$mail->SMTPSecure = 'tls';
		$mail->From = 'pat@pb119.net';
		$mail->FromName = 'Roomr';
		$mail->addAddress ( $emailAddress, 'Meeting Participant' );
		$mail->isHTML ( true );
		$mail->Subject = 'You are invited to a meeting';
		$mail->Body = "You are invited to join <a href='{$link}'>a meeting</a>";
		if (! $mail->send ()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}
		break;
	
	case "editReservation" :
		if (isset ( $params ["reservationId"] )) {
			$query = "UPDATE Reservation SET ";
			$p = array ();
			if (isset ( $params ["roomId"], $params ["date"], $params ["startTime"], $params ["endTime"] )) {
				if (strlen ( $params ["startTime"] ) == 5)
					$params ["startTime"] = $params ["startTime"] . ":00";
				if (strlen ( $params ["endTime"] ) == 5)
					$params ["endTime"] = $params ["endTime"] . ":00";
				$startDateTime = $params ["date"] . " " . $params ["startTime"];
				$endDateTime = $params ["date"] . " " . $params ["endTime"];
				$query = $query . "roomId=?, startTime=?, endTime=?";
				$p [] = $params ["roomId"];
				$p [] = $startDateTime;
				$p [] = $endDateTime;
			}
			if (isset ( $params ["subject"], $params ["description"] )) {
				$query = $query . "subject=?, description=?";
				$p [] = $params ["subject"];
				$p [] = $params ["description"];
			}
			$query = $query . " WHERE reservationId=?";
			$p [] = $params ["reservationId"];
			$result = sqlsrv_query ( $conn, $query, $p );
			if (! $result) {
				$returnData ["success"] = false;
			} else {
				$returnData ["success"] = true;
			}
		} else {
			$returnData ["success"] = false;
		}
		break;
	
	case "deleteReservation" :
		$p = array (
				$params ["reservationId"] 
		);
		$query1 = "DELETE FROM ReservationUser WHERE reservationId=?";
		$result1 = sqlsrv_query ( $conn, $query1, $p );
		$query2 = "DELETE FROM EmailId WHERE reservationId=?";
		$result2 = sqlsrv_query ( $conn, $query2, $p );
		$query3 = "DELETE FROM Reservation WHERE reservationId=?";
		$result3 = sqlsrv_query ( $conn, $query3, $p );
		if (! $result1 || ! $result2 || ! $result3) {
			$returnData ["success"] = false;
		} else {
			$returnData ["success"] = true;
		}
	case "joinMeeting" :
		
		// not used
		break;
	
	case "leaveMeeting" :
		$p = array (
				$userId,
				$params ["reservationId"] 
		);
		$query = "EXEC removeReservationUser @userId=?, @reservationId=?;";
		$result = sqlsrv_query ( $conn, $query, $p );
		if (! $result) {
			$returnData ["success"] = false;
		} else {
			$returnData ["success"] = true;
		}
		break;
	
	default :
		
		break;
}

sqlsrv_close ( $conn );
echo (json_encode ( $returnData ));

// ADDITIONAL FUNCTIONS
function oneRow($conn,$q, $p) {
	$result = sqlsrv_query ( $conn, $q, $p );
	return sqlsrv_fetch_array ( $result, SQLSRV_FETCH_ASSOC );
}
?>
